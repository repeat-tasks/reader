import java.io.*;
import java.util.*;

public class Main {
    public static void main(String[] args) {
        
        try (Scanner sc = new Scanner(new File("index.txt"));
             BufferedWriter bw = new BufferedWriter(new FileWriter("output.txt"))) {

            Set<String> resultSet = new HashSet<>();

            List<String> nList = new ArrayList<>();
            List<String> mList = new ArrayList<>();

            short n = sc.nextShort();
            sc.nextLine();
            addToListValues(sc, nList, n);

            short m = sc.nextShort();
            sc.nextLine();
            addToListValues(sc, mList, m);

            for (String s : nList) {
                for (String value : mList) {
                    String[] split = value.split(" ");
                    for (String item : split) {
                        if (s.contains(item) || s.contains("Бетон") && value.contains("бетон") || value.contains("Цемент")) {
                            resultSet.add(s + ":" + value);
                        }
                    }
                }
            }

            endFiltersForArrays(resultSet, nList);

            endFiltersForArrays(resultSet, mList);

            for (String item : resultSet) {
                bw.write(item);
                bw.newLine();
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private static void endFiltersForArrays(Set<String> resultSet, List<String> nList) {
        for (int i = 0; i < resultSet.size(); i++) {
            for (String value : nList) {
                String[] split = value.split(" ");
                for (int k = 0; k < split.length; k++) {
                    int finalK = k;
                    if (resultSet.stream().noneMatch(s -> s.contains(split[finalK]))) {
                        resultSet.add(value + ":?");
                    }
                }
            }
        }
    }

    private static void addToListValues(Scanner sc, List<String> mList, short m) {
        for (int i = 0; i < m; i++) {
            String s = sc.nextLine();
            mList.add(s);
        }
    }
}
